$(document).ready(function () {

  $('.s-hamburger').on('click', function () {
    $('.s-mobile-menu').addClass('s-mobile-menu--is-shown');
    $('body').addClass('mobile-menu-is-open');
  });

  $('.s-mobile-menu__close').on('click', function () {
    $('.s-mobile-menu').removeClass('s-mobile-menu--is-shown');
    $('body').removeClass('mobile-menu-is-open');
  });

  $('.s-mobile-menu__nav-list > li > a').on('click', function (e) {

    if ($(this).siblings().length != 0) {
      e.preventDefault();
      $(this).siblings('ul').toggleClass('is-open');
    };
  });

  $(".s-sidebar__documents").lightGallery({
    selector: 'a',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false
  });
});